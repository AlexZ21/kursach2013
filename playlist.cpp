#include "playlist.h"

PlayList::PlayList(QWidget *parent) :
    QListWidget(parent)
{
    size = 0;
    offset = 0;
    setFrameStyle(QFrame::NoFrame | QFrame::Raised);
    setStyleSheet("QListWidget { background: rgba(255,255,255,150); padding: 10px;}"
                  "QListWidget::item { height: 25px; }"
                  "QListWidget::item::text { color: rgba(50,50,50,255); }"
                  "QListView::item:hover {background: url(:/data/playlist_hover.png) no-repeat; padding-left: 10px;}"
                  "QListView::item:selected:active {background: url(:/data/playlist_hover.png) no-repeat; padding-left: 10px;}"
                  "QListView::item:selected:!active {background: url(:/data/playlist_hover.png) no-repeat; padding-left: 10px;}");
    setFont(QFont("a_CityNova",14));
    connect(this, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(rowClicked(QListWidgetItem*)));
    connect(this, SIGNAL(itemDoubleClicked(QListWidgetItem*)), this, SLOT(rowDoubleClicked(QListWidgetItem*)));


    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    vertScrollBar = this->verticalScrollBar();
    vertScrollBar->setStyleSheet("QScrollBar:vertical  {"
                                 "background: rgba(190,190,190,0); "
                                 "width: 15px}"
                                 "QScrollBar::add-line:vertical  {"
                                 "background: rgba(190,190,190,0); "
                                 "width: 0px;"
                                 "height: 0px;}"
                                 "QScrollBar::sub-line:vertical  {"
                                 "background: rgba(190,190,190,0); "
                                 "width: 0px;"
                                 "height: 0px;}"
                                 "QScrollBar::handle:vertical  {"
                                 "background: rgba(0,0,0,100);"
                                 "margin: 5px;"
                                 "min-height: 20px;"
                                 "width: 10px;}"
                                 "QScrollBar::handle:vertical:hover  {"
                                 "background: rgba(50,50,50,240);"
                                 "margin: 5px;"
                                 "min-height: 20px;"
                                 "width: 10px;}");

    connect(vertScrollBar, SIGNAL(valueChanged(int)), this, SLOT(scrollBarChanged(int)));
}

void PlayList::setPlayListFromAudio(AVKReply &reply)
{
    QDomElement rootElement = reply.document.documentElement();
    QDomNode node = rootElement.firstChild();

    if(reply.query.queryItemValue("offset").toInt() == 0)
    {
        offset = 0;
        playList.clear();
        vertScrollBar->setSliderPosition(0);
    }

    size = node.toElement().text().toInt();
    method = reply.method;
    this->id = reply.ownerId;
    node = node.nextSibling().firstChild();

    while(!node.isNull())
    {
        QDomNode childNode = node.firstChild();
        PlayListItem playListItem;

        playListItem.audioId = childNode.toElement().text();

        childNode = childNode.nextSibling();
        playListItem.ownerId = childNode.toElement().text();

        childNode = childNode.nextSibling();
        playListItem.artist = childNode.toElement().text();

        childNode = childNode.nextSibling();
        playListItem.title = childNode.toElement().text();

        childNode = childNode.nextSibling().nextSibling();
        QUrl url(childNode.toElement().text());
        playListItem.url = url.scheme() + "://" + url.host() + url.path();

        playList.push_back(playListItem);

        node = node.nextSibling();
    }

    updatePlayList();
}

void PlayList::setPlayListFromSearch(AVKReply &reply)
{
    QDomElement rootElement = reply.document.documentElement();
    QDomNode node = rootElement.firstChild();

    if(reply.query.queryItemValue("offset").toInt() == 0)
    {
        offset = 0;
        playList.clear();
        vertScrollBar->setSliderPosition(0);
    }

    size = 1000;
    q = reply.query.queryItemValue("q");

    method = reply.method;
    this->id = reply.ownerId;
    node = node.nextSibling().firstChild();

    while(!node.isNull())
    {
        QDomNode childNode = node.firstChild();
        PlayListItem playListItem;

        playListItem.audioId = childNode.toElement().text();

        childNode = childNode.nextSibling();
        playListItem.ownerId = childNode.toElement().text();

        childNode = childNode.nextSibling();
        playListItem.artist = childNode.toElement().text();

        childNode = childNode.nextSibling();
        playListItem.title = childNode.toElement().text();

        childNode = childNode.nextSibling().nextSibling();
        QUrl url(childNode.toElement().text());
        playListItem.url = url.scheme() + "://" + url.host() + url.path();

        playList.push_back(playListItem);

        node = node.nextSibling();
    }

    updatePlayList();
}

void PlayList::setPlayListFromWall(AVKReply &reply)
{
    QDomElement rootElement = reply.document.documentElement();
    QDomNode node = rootElement.firstChild();
    size = node.toElement().text().toInt();
    qDebug() << size;
    if(reply.query.queryItemValue("offset").toInt() == 0)
    {
        offset = 0;
        playList.clear();
        vertScrollBar->setSliderPosition(0);
    }
    method = reply.method;
    this->id = reply.ownerId;
    offset += reply.query.queryItemValue("count").toInt();
    qDebug() << offset;
    node = node.nextSibling().firstChild();


    while(!node.isNull())
    {
        QDomNodeList audioNodeList = node.toElement().elementsByTagName("attachment");

        for(int i = 0; i<audioNodeList.size(); ++i)
        {

            QDomNode childNode = audioNodeList.item(i).toElement().elementsByTagName("audio").item(0).firstChild();
            if(!childNode.isNull())
            {
                PlayListItem playListItem;

                playListItem.audioId = childNode.toElement().text();

                childNode = childNode.nextSibling();
                playListItem.ownerId = childNode.toElement().text();

                childNode = childNode.nextSibling();
                playListItem.artist = childNode.toElement().text();

                childNode = childNode.nextSibling();
                playListItem.title = childNode.toElement().text();

                childNode = childNode.nextSibling().nextSibling();
                QUrl url(childNode.toElement().text());
                playListItem.url = url.scheme() + "://" + url.host() + url.path();

                playList.push_back(playListItem);


            }
        }

        node = node.nextSibling();
    }

    int count;
    if(playList.size() < 50 && offset < size)
    {
        if((offset+40) <= size)
            count = 40;
        else
            count = size-offset;



        AVKManager::get(method,"owner_id="+id+"&count="+QString::number(count)+"&offset="+QString::number(offset));
    }

    updatePlayList();
}

void PlayList::setPlayListFromNews(AVKReply &reply)
{
    QDomElement rootElement = reply.document.documentElement();
    QDomNode node = rootElement.firstChild();
    size = 1000;
    if(reply.query.queryItemValue("offset").toInt() == 0)
    {
        offset = 0;
        playList.clear();
        vertScrollBar->setSliderPosition(0);
    }
    method = reply.method;
    this->id = reply.ownerId;
    offset += reply.query.queryItemValue("count").toInt();


    while(!node.isNull())
    {
        QDomNodeList audioNodeList = node.toElement().elementsByTagName("attachment");

        for(int i = 0; i<audioNodeList.size(); ++i)
        {

            QDomNode childNode = audioNodeList.item(i).toElement().elementsByTagName("audio").item(0).firstChild();
            if(!childNode.isNull())
            {
                PlayListItem playListItem;

                playListItem.audioId = childNode.toElement().text();

                childNode = childNode.nextSibling();
                playListItem.ownerId = childNode.toElement().text();

                childNode = childNode.nextSibling();
                playListItem.artist = childNode.toElement().text();

                childNode = childNode.nextSibling();
                playListItem.title = childNode.toElement().text();

                childNode = childNode.nextSibling().nextSibling();
                QUrl url(childNode.toElement().text());
                playListItem.url = url.scheme() + "://" + url.host() + url.path();

                playList.push_back(playListItem);


            }
        }

        node = node.nextSibling();
    }

    int count;
    if(playList.size() < 50 && offset < size)
    {
        if((offset+40) <= size)
            count = 40;
        else
            count = size-offset;



        AVKManager::get(method,"owner_id="+id+"&count="+QString::number(count)+"&offset="+QString::number(offset));
    }

    updatePlayList();
}

void PlayList::updatePlayList()
{
    clear();
    for(int i = 0; i < playList.size(); i++)
    {
        addItem(QString::number(i+1)+". "+playList[i].artist + " - " + playList[i].title);
    }
    vertScrollBar->blockSignals(false);
}

void PlayList::rowClicked(QListWidgetItem *item)
{
    int row = currentRow();
    emit selectTrack(playList[row]);
    qDebug() << playList[row].artist << " - " << playList[row].title;
}

void PlayList::rowDoubleClicked(QListWidgetItem *item)
{
    int row = currentRow();
    AVKManager::get("audio.search","q="+playList[row].artist+"&count=200&offset=0");
}

void PlayList::scrollBarChanged(int i)
{
    if(offset < size)
        if(i == vertScrollBar->maximum())
        {
            vertScrollBar->blockSignals(true);
            QString c;

            if(method == "audio.get")
            {
                offset = count();
                if((offset + 100) <= size)
                    c = "100";
                else
                    c = QString::number(size-offset);
                AVKManager::get(method,"owner_id="+id+"&count="+c+"&offset="+QString::number(offset));
            }

            if(method == "audio.search")
            {
                offset = count();
                if((offset + 200) <= size)
                    c = "200";
                else
                    c = QString::number(size-offset);
                AVKManager::get(method,"q="+q+"&count="+c+"&offset="+QString::number(offset));
            }

            if(method == "wall.get")
            {
                int count;

                if((offset+40) <= size)
                    count = 40;
                else
                    count = size-offset;

                AVKManager::get(method,"owner_id="+id+"&count="+QString::number(count)+"&offset="+QString::number(offset));

            }

            if(method == "newsfeed.get")
            {
                int count;

                if((offset+40) <= size)
                    count = 40;
                else
                    count = size-offset;

                AVKManager::get(method,"owner_id="+id+"&count="+QString::number(count)+"&offset="+QString::number(offset));

            }
        }
}

void PlayList::nextTrack(bool shuffle)
{
    if(!shuffle)
    {
        int row = currentRow();
        if((row+1) < playList.size())
        {
            emit selectTrack(playList[row+1]);
            setCurrentRow(row+1);
        }
    }else{
        int row = qrand()%playList.size();
        emit selectTrack(playList[row]);
        setCurrentRow(row);
    }
}

void PlayList::previousTrack(bool shuffle)
{
    if(!shuffle)
    {
        int row = currentRow();
        if((row-1) >= 0)
        {
            emit selectTrack(playList[row-1]);
            setCurrentRow(row-1);
        }
    }else{
        int row = qrand()%playList.size();
        emit selectTrack(playList[row]);
        setCurrentRow(row);
    }
}
