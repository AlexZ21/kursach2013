#include "avkmanager.h"

AVKManager AVKManager::manager;

void AVKManager::setToken(QString token)
{
    manager.token = token;
    QFile file;
    file.setFileName("token");
    file.open(QIODevice::WriteOnly);
    QTextStream text(&file);
    text << token;
    file.close();
}

void AVKManager::setId(QString id)
{
    manager.id = id;
}

bool AVKManager::checkToken()
{
    QFile file;
    file.setFileName("token");
    file.open(QIODevice::ReadOnly);
    QTextStream text(&file);
    manager.token = text.readLine();
    file.close();

    if(manager.token.isEmpty())
        return false;
    else
        return true;
}

void AVKManager::clearToken()
{
    QFile file;
    file.setFileName("token");
    file.open(QIODevice::WriteOnly);
    QTextStream text(&file);
    text <<"";
    file.close();
    manager.token = "";
}

void AVKManager::get(QString method, QString param)
{

}

AVKManager *AVKManager::getManager()
{
    return &manager;
}

AVKManager::AVKManager()
{
    qDebug() << "Init VK manager";
}

AVKManager::~AVKManager()
{
    qDebug() << "Destroy VK manager";
}
