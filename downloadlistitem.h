#ifndef DOWNLOADLISTITEM_H
#define DOWNLOADLISTITEM_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QProgressBar>
#include <QDebug>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QFileDialog>
#include <QTime>
#include <QTimer>

class DownloadListItem : public QWidget
{
    Q_OBJECT
    QWidget *bodyWidget;
    QVBoxLayout *mainLayout;
    QVBoxLayout *bodyLayout;

    QString artist;
    QString title;
    QString url;

    QNetworkAccessManager *manager;
    QString fileName;
    qint64 targetFileSize;
    qint64 recievedSize;

    QLabel *numberInList;
    QLabel *trackName;
    QProgressBar *progressBar;

    int time;
    QTimer timer;
    QString timerString;
public:
    explicit DownloadListItem(QString num, QString art, QString tit, QString url, QWidget *parent = 0);
    bool downloaded;
    QString filePath;
    QLabel *loadStatus;
signals:

public slots:
    void downloadFinished(QNetworkReply* data);
    void downloadProgress(qint64 recieved1, qint64 total);
    void tick();
};

#endif // DOWNLOADLISTITEM_H
