#include "authframe.h"

AuthFrame::AuthFrame(AFrame *parentFrame, QWidget *parent) :
    AFrame(parentFrame, parent)
{
    hideHeaderIcon(true);
    getHeaderTitle()->setText("АВТОРИЗАЦИЯ");
    setDeleteOnClose(true);

    AConnectDialog *conDialog = new AConnectDialog(this);
    getMainLayout()->addWidget(conDialog,1);
    connect(conDialog, SIGNAL(connected(QString,QString)),this, SLOT(conected(QString,QString)));
    conDialog->loadLoginPage();
}

void AuthFrame::conected(QString token, QString id)
{
    AVKManager::setToken(token);
    AVKManager::setId(id);
    emit connected();
    close();
}
