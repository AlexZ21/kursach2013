#include "menulist.h"
#include <qalgorithms.h>

bool prefLessThan(const MenuListItem &p1, const MenuListItem &p2)
{
    return p1.data[0] < p2.data[0];
}

MenuList::MenuList(QWidget *parent) :
    QListWidget(parent)
{
    setFixedWidth(200);
    setFrameStyle(QFrame::NoFrame | QFrame::Raised);
    setStyleSheet("QListWidget { background: rgba(0,0,0,70); }"
                  "QListWidget::item { height: 30px; }"
                  "QListWidget::item::text { color: rgba(255,255,255,255); }"
                  "QListView::item:hover {background: url(:/data/menu_hover.png); padding-left: 10px;}"
                  "QListView::item:selected:active {background: url(:/data/menu_selected.png); padding-left: 10px;}"
                  "QListView::item:selected:!active {background: url(:/data/menu_selected.png); padding-left: 10px;}");
    connect(this, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(rowClicked(QListWidgetItem*)));
    setCurrentRow(0);
    setFont(QFont("a_CityNova",14,QFont::Bold));

    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);

    QScrollBar *menuListVerticalScrollBar = verticalScrollBar();

    menuListVerticalScrollBar->setStyleSheet("QScrollBar:vertical  {"
                                             "background: rgba(190,190,190,0); "
                                             "width: 15px}"
                                             "QScrollBar::add-line:vertical  {"
                                             "width: 0px;"
                                             "height: 0px;}"
                                             "QScrollBar::sub-line:vertical  {"
                                             "width: 0px;"
                                             "height: 0px;}"
                                             "QScrollBar::handle:vertical  {"
                                             "background: rgba(255,255,255,100);"
                                             "margin: 5px;"
                                             "width: 10px;}"
                                             "QScrollBar::handle:vertical:hover  {"
                                             "background: rgba(255,255,255,240);"
                                             "margin: 5px;"
                                             "width: 10px;}");

    setMenu();
    updateMenuList();
}

void MenuList::setMenu()
{
    QVector<MenuListItem> mainMenu;

    MenuListItem wall;
    wall.type = "wall";
    wall.data.push_back("СТЕНА");
    mainMenu.push_back(wall);


    MenuListItem myaudio;
    myaudio.type = "myaudio";
    myaudio.data.push_back("МОИ АУДИОЗАПИСИ");
    mainMenu.push_back(myaudio);


    MenuListItem friends;
    friends.type = "friends";
    friends.data.push_back("ДРУЗЬЯ");
    mainMenu.push_back(friends);


    MenuListItem groups;
    groups.type = "groups";
    groups.data.push_back("СООБЩЕСТВА");
    mainMenu.push_back(groups);

    MenuListItem news;
    news.type = "news";
    news.data.push_back("НОВОСТИ");
    mainMenu.push_back(news);


    stackList.push_back(mainMenu);
}

void MenuList::updateMenuList()
{
    blockSignals(false);

    clear();
    QVector<MenuListItem> list = stackList.top();

    for(int i = 0; i < list.size(); i++)
    {
        addItem(list[i].data[0]);
    } 
}

void MenuList::setGroupsList(QDomNode node)
{
    QVector<MenuListItem> mainMenu;


    while(!node.isNull())
    {
        QDomNode childNode = node.firstChild();
        QString idGroup = "-"+childNode.toElement().text();
        childNode = childNode.nextSibling();
        QString firstName = childNode.toElement().text();

        MenuListItem item;
        item.type = "groupsListItem";
        item.data.push_back(firstName);
        item.data.push_back(idGroup);
        mainMenu.push_back(item);

        node = node.nextSibling();
    }

    qSort(mainMenu.begin(), mainMenu.end(), prefLessThan);

    MenuListItem back;
    back.type = "back";
    back.data.push_back("...НАЗАД");
    mainMenu.push_front(back);

    stackList.push_back(mainMenu);
    updateMenuList();
}

void MenuList::setFriendsList(QDomNode node)
{
    QVector<MenuListItem> mainMenu;

    while(!node.isNull())
    {
        QDomNode childNode = node.firstChild();
        QString idFiend = childNode.toElement().text();
        childNode = childNode.nextSibling();
        QString firstName = childNode.toElement().text();
        childNode = childNode.nextSibling();
        QString lastName = childNode.toElement().text();

        MenuListItem item;
        item.type = "friendsListItem";
        item.data.push_back(firstName+QString(" ")+lastName);
        item.data.push_back(idFiend);
        mainMenu.push_back(item);

        node = node.nextSibling();
    }
    qSort(mainMenu.begin(), mainMenu.end(), prefLessThan);

    MenuListItem back;
    back.type = "back";
    back.data.push_back("...НАЗАД");
    mainMenu.push_front(back);//.push_back(back);

    stackList.push_back(mainMenu);
    updateMenuList();
}

void MenuList::setSubMenuList(QString id)
{
    QVector<MenuListItem> mainMenu;

    MenuListItem back;
    back.type = "back";
    back.data.push_back("...НАЗАД");
    mainMenu.push_back(back);

    MenuListItem subAudio;
    subAudio.type = "subAudio";
    subAudio.data.push_back("АУДИОЗАПИСИ");
    subAudio.data.push_back(id);
    mainMenu.push_back(subAudio);

    MenuListItem subWall;
    subWall.type = "subWall";
    subWall.data.push_back("СТЕНА");
    subWall.data.push_back(id);
    mainMenu.push_back(subWall);

    stackList.push_back(mainMenu);
    updateMenuList();
}

void MenuList::addTestList()
{
    QVector<MenuListItem> mainMenu;

    MenuListItem back;
    back.type = "back";
    back.data.push_back("...НАЗАД");
    mainMenu.push_back(back);

    for(int i = 0; i < 10; i++)
    {
        MenuListItem wall;
        wall.type = "test";
        wall.data.push_back(QString("Test ")+QString::number(i));
        mainMenu.push_back(wall);
    }


    stackList.push_back(mainMenu);
    updateMenuList();
}

void MenuList::rowClicked(QListWidgetItem *item)
{
    blockSignals(true);

    int row = currentRow();
    QVector<MenuListItem> list = stackList.top();

    if(list[row].type == "back")
    {
        stackList.pop();
        updateMenuList();
    }

    if(list[row].type == "groups")
    {
        AVKManager::get("groups.get","extended=1");
    }

    if(list[row].type == "friends")
    {
        AVKManager::get("friends.get","fields=screen_name");
    }

    if(list[row].type == "friendsListItem" || list[row].type == "groupsListItem")
    {
        setSubMenuList(list[row].data[1]);
    }

    if(list[row].type == "myaudio")
    {
        AVKManager::get("audio.get","owner_id="+AVKManager::getId()+"&count=100&offset=0");
    }

    if(list[row].type == "subAudio")
    {
        AVKManager::get("audio.get","owner_id="+list[row].data[1]+"&count=100&offset=0");
    }

    if(list[row].type == "subWall")
    {
        AVKManager::get("wall.get","owner_id="+list[row].data[1]+"&count=40&offset=0");
    }

    if(list[row].type == "wall")
    {
        AVKManager::get("wall.get","owner_id="+AVKManager::getId()+"&count=40&offset=0");
    }

    if(list[row].type == "news")
    {
        AVKManager::get("newsfeed.get","owner_id="+AVKManager::getId()+"&filters=post&count=40&offset=0");
    }


    if(list[row].type == "test")
    {
        addTestList();
    }


    qDebug() << list[row].data[0];
}
