#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
{
    QFontDatabase::addApplicationFont(":/fonts/f2.ttf");
    int id = QFontDatabase::addApplicationFont(":/fonts/f1.ttf");
    //int id2 = QFontDatabase::addApplicationFont(":/fonts/f2.ttf");




    QString family = QFontDatabase::applicationFontFamilies(id).at(0);
    qDebug() << family;


    centralWidget = new QWidget(this);
    mainLayout = new QVBoxLayout(centralWidget);
    mainLayout->setContentsMargins(0,0,0,0);


    mainFrame = new MainFrame(NULL, this);
    mainFrame->setInLayout(mainLayout);

    setSizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
    setStyleSheet("QMainWindow { background: url(:/data/background.jpg); }");
    setWindowTitle("Codein v.1.0");
    setCentralWidget(centralWidget);
    resize(800,480);
}

MainWindow::~MainWindow()
{

}
