#include "audioplayer.h"

AudioPlayer::AudioPlayer(QWidget *parent) :
    QWidget(parent)
{
    mainLayout = new QVBoxLayout(this);
    mainLayout->setContentsMargins(0,0,0,0);
    mainLayout->setSpacing(0);
    bodyWidget = new QWidget(this);
    mainLayout->addWidget(bodyWidget);
    bodyWidget->setStyleSheet("QWidget {background: url(:/data/player_background.png);}"
                              "QWidget {border-width: 0px; border-image: url(:/data/shadow.png);}");
    setFixedHeight(90);

    QVBoxLayout *vLayout = new QVBoxLayout(bodyWidget);
    vLayout->setContentsMargins(7,7,7,7);

    QHBoxLayout *trackControlLayout = new QHBoxLayout();
    trackControlLayout->setContentsMargins(0,0,0,0);
    vLayout->addLayout(trackControlLayout);

    timeLabel = new QLabel("--:--");
    timeLabel->setStyleSheet("color: rgba(255,255,255,255); background: rgba(235,235,235,0); border-width: 0px; border-image: none;");
    timeLabel->setFont(QFont("a_CityNova",12));

    timeLeftLabel = new QLabel("--:--");
    timeLeftLabel->setStyleSheet("color: rgba(255,255,255,255); background: rgba(235,235,235,0); border-width: 0px; border-image: none;");
    timeLeftLabel->setFont(QFont("a_CityNova",12));

    sliderControl = new QSlider();
    sliderControl->setOrientation(Qt::Horizontal);
    sliderControl->setFixedHeight(22);
    sliderControl->setStyleSheet("* {"
                                 "background: rgba(235,235,235,0); border-width: 0px; border-image: none;}"
                                 "QSlider::groove:horizontal {"
                                 "height: 2px;"
                                 "background: rgba(235,235,235,255);"
                                 "}"
                                 "QSlider::handle:horizontal {"
                                 "background: url(:/data/control/scrubber.control.normal.png);"
                                 "width: 22px;"
                                 "height: 22px;"
                                 "margin: -10px 1px;"
                                 "border-radius: 11px;"
                                 "}"
                                 "QSlider::handle:horizontal:pressed {"
                                 "background: url(:/data/control/scrubber.control.pressed.png);"
                                 "width: 22px;"
                                 "height: 22px;"
                                 "margin: -10px 1px;"
                                 "border-radius: 11px;"
                                 "}"
                                 " QSlider::sub-page:horizontal {"
                                 "background: rgba(0,182,255,255);"
                                 "}");

    connect(sliderControl, SIGNAL(sliderReleased()), this, SLOT(sliderControlRelease()));
    connect(sliderControl, SIGNAL(sliderPressed()), this, SLOT(sliderControlPress()));
    connect(sliderControl, SIGNAL(sliderMoved(int)), this, SLOT(sliderControlMoved(int)));


    trackControlLayout->addWidget(timeLabel,0);
    trackControlLayout->addWidget(sliderControl,1);
    trackControlLayout->addWidget(timeLeftLabel,0);

    QHBoxLayout *controlLayout = new QHBoxLayout();
    controlLayout->setContentsMargins(0,0,0,0);
    vLayout->addLayout(controlLayout);

    cover = new AImage();
    cover->loadFromFile(":/data/cover");
    cover->setFixedSize(50,50);
    controlLayout->addWidget(cover,0,Qt::AlignLeft);

    QVBoxLayout *songTitleLayout = new QVBoxLayout();
    songTitleLayout->setContentsMargins(0,0,0,0);
    songTitleLayout->setSpacing(1);
    controlLayout->addLayout(songTitleLayout);

    artistLabel = new QLabel("ИСПОЛНИТЕЛЬ");
    artistLabel->setStyleSheet("color: rgba(255,255,255,255); background: rgba(235,235,235,0); border-width: 0px; border-image: none;");
    artistLabel->setFont(QFont("a_CityNova",14,QFont::Bold));
    artistLabel->setMinimumWidth(200);

    titleLabel = new QLabel("НАЗВАНИЕ ПЕСНИ");
    titleLabel->setStyleSheet("color: rgba(255,255,255,255); background: rgba(235,235,235,0); border-width: 0px; border-image: none;");
    titleLabel->setFont(QFont("a_CityNova",14));
    titleLabel->setMinimumWidth(200);

    songTitleLayout->addWidget(titleLabel,0,Qt::AlignLeft | Qt::AlignVCenter);
    songTitleLayout->addWidget(artistLabel,0,Qt::AlignLeft | Qt::AlignVCenter);

    controlLayout->addStretch(1);


    downloadButton = addButton(":/data/control/download", QSize(34,34));
    controlLayout->addWidget(downloadButton,0,Qt::AlignRight | Qt::AlignVCenter);
    connect(downloadButton,SIGNAL(clicked()),this, SLOT(downloadButtonClicked()));

    addAudioButton = addButton(":/data/control/add", QSize(34,34));
    controlLayout->addWidget(addAudioButton,0,Qt::AlignRight | Qt::AlignVCenter);
    connect(addAudioButton,SIGNAL(clicked()),this, SLOT(addAudioButtonClicked()));

    AImage *sep = new AImage();
    sep->loadFromFile(":/data/sep");
    sep->setFixedSize(2,30);
    controlLayout->addWidget(sep,0,Qt::AlignRight | Qt::AlignVCenter);

    shuffleButton = addButton(":/data/control/shuffle", QSize(34,34));
    shuffleButton->setCheckable(true);
    controlLayout->addWidget(shuffleButton,0,Qt::AlignRight | Qt::AlignVCenter);
    connect(shuffleButton,SIGNAL(changeState(bool)),this, SLOT(shuffleButtonClicked(bool)));

    previousButton = addButton(":/data/control/previous", QSize(40,40));
    controlLayout->addWidget(previousButton,0,Qt::AlignRight | Qt::AlignVCenter);
    connect(previousButton,SIGNAL(clicked()),this, SLOT(previousButtonClicked()));

    playButton = addButton(":/data/control/play", QSize(50,50));
    controlLayout->addWidget(playButton,0,Qt::AlignRight | Qt::AlignTop);
    connect(playButton,SIGNAL(clicked()),this, SLOT(playButtonClicked()));

    pauseButton = addButton(":/data/control/pause", QSize(50,50));
    controlLayout->addWidget(pauseButton,0,Qt::AlignRight | Qt::AlignVCenter);
    pauseButton->hide();
    connect(pauseButton,SIGNAL(clicked()),this, SLOT(pauseButtonClicked()));

    nextButton = addButton(":/data/control/next", QSize(40,40));
    controlLayout->addWidget(nextButton,0,Qt::AlignRight | Qt::AlignVCenter);
    connect(nextButton,SIGNAL(clicked()),this, SLOT(nextButtonClicked()));

    repeatButton = addButton(":/data/control/repeat", QSize(34,34));
    repeatButton->setCheckable(true);
    controlLayout->addWidget(repeatButton,0,Qt::AlignRight | Qt::AlignVCenter);
    connect(repeatButton,SIGNAL(changeState(bool)),this, SLOT(repeatButtonClicked(bool)));


    mediaPlayer = new QMediaPlayer(this);
    mediaPlayer->setVolume(100);
    connect(mediaPlayer, SIGNAL(positionChanged(qint64)), this, SLOT(positionChanged(qint64)));

    sliderPressed = false;
    repeatTrack = false;
    randomTrack = false;

}

void AudioPlayer::setTrack(PlayListItem track)
{
    this->track = track;
    artistLabel->setText(track.artist.toUpper());
    titleLabel->setText(track.title.toUpper());
    mediaPlayer->setMedia(QUrl(track.url));
    playButtonClicked();
    addAudioButton->show();
}

void AudioPlayer::playButtonClicked()
{
    pauseButton->show();
    playButton->hide();
    mediaPlayer->play();
}

void AudioPlayer::pauseButtonClicked()
{
    playButton->show();
    pauseButton->hide();
    mediaPlayer->pause();
}

void AudioPlayer::nextButtonClicked()
{
    emit nextTrack(randomTrack);
}

void AudioPlayer::previousButtonClicked()
{
    emit previousTrack(randomTrack);
}

void AudioPlayer::repeatButtonClicked(bool state)
{
    repeatTrack = state;
}

void AudioPlayer::shuffleButtonClicked(bool state)
{
    randomTrack = state;
}

void AudioPlayer::addAudioButtonClicked()
{
 if(!track.audioId.isEmpty())
 {
     AVKManager::get("audio.add","audio_id="+track.audioId+"&owner_id="+track.ownerId);
     addAudioButton->hide();
 }
}

void AudioPlayer::downloadButtonClicked()
{
    emit newDownload(track.artist, track.title, track.url);
}

void AudioPlayer::positionChanged(qint64 pos)
{
    if(mediaPlayer->mediaStatus() == QMediaPlayer::LoadingMedia ||
            mediaPlayer->mediaStatus() == QMediaPlayer::NoMedia ||
            mediaPlayer->mediaStatus() == QMediaPlayer::UnknownMediaStatus ||
            mediaPlayer->mediaStatus() == QMediaPlayer::InvalidMedia)
    {
        timeLabel->setText("00:00");
        timeLeftLabel->setText("00:00");
        sliderControl->setSliderPosition(0);
    }else{
        //if(loadingCircle->isActive())
        //{
        //    loadingCircle->stop();
        //    loadingCircle->hide();
        //}

        QString sek;
        QString allSek;

        if((sliderControl->sliderPosition())%60 < 10)
            sek = QString("0") + QString::number((sliderControl->sliderPosition())%60);
        else
            sek = QString::number((sliderControl->sliderPosition())%60);

        if((mediaPlayer->duration()/1000)%60 < 10)
            allSek = QString("0") + QString::number((mediaPlayer->duration()/1000)%60);
        else
            allSek = QString::number((mediaPlayer->duration()/1000)%60);

        if(!sliderPressed) timeLabel->setText(QString(QString::number((sliderControl->sliderPosition())/60) + ":" + sek));
        timeLeftLabel->setText(QString(QString::number((mediaPlayer->duration()/1000)/60) + ":" + allSek));
        sliderControl->setMaximum(mediaPlayer->duration()/1000);
        if(!sliderPressed) sliderControl->setSliderPosition(pos/1000);


        if(pos/1000 == mediaPlayer->duration()/1000-1) {
            mediaPlayer->stop();
            if(repeatTrack)
            {
                mediaPlayer->setPosition(0);
            }else{
                nextButtonClicked();
            }
        }
    }
}

void AudioPlayer::sliderControlPress()
{
    sliderPressed = true;
    timeLabel->setText(QString(QString::number((sliderControl->sliderPosition())/60) + ":" + QString::number((sliderControl->sliderPosition())%60)));
}

void AudioPlayer::sliderControlRelease()
{
    mediaPlayer->setPosition(sliderControl->sliderPosition()*1000);
    sliderPressed = false;
    if(sliderControl->sliderPosition() == sliderControl->maximum())
        nextButtonClicked();
}

void AudioPlayer::sliderControlMoved(int pos)
{
    timeLabel->setText(QString(QString::number((sliderControl->sliderPosition())/60) + ":" + QString::number((sliderControl->sliderPosition())%60)));
}

AButton *AudioPlayer::addButton(QString iconPath, QSize size)
{
    AButton *but = new AButton();
    but->setImage(iconPath);
    but->setButtonType(AudioPlayerControl);
    but->setFixedSize(size);
    but->setImageSize(size);
    return but;
}
