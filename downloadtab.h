#ifndef DOWNLOADTAB_H
#define DOWNLOADTAB_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QListWidget>
#include <QScrollBar>
#include <QDesktopServices>
#include "downloadlistitem.h"
#include "AFramework/abutton.h"

class DownloadTab : public QWidget
{
    Q_OBJECT
    QWidget *bodyWidget;
    QVBoxLayout *mainLayout;
    QVBoxLayout *bodyLayout;
    QListWidget *listWidget;
    AButton *clenButton;
public:
    explicit DownloadTab(QWidget *parent = 0);

signals:

public slots:
    void addDownload(QString artist, QString title, QString url);
    void itemClicked(QListWidgetItem* item);
      void cleanList();

};

#endif // DOWNLOADTAB_H
