#ifndef AUDIOPLAYER_H
#define AUDIOPLAYER_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include <QSlider>
#include <QtMultimedia/QMediaPlayer>

#include <AFramework/abutton.h>
#include <AFramework/aimage.h>
#include <playlist.h>

class AudioPlayer : public QWidget
{
    Q_OBJECT
    QWidget *bodyWidget;
    QVBoxLayout *mainLayout;

    QLabel *timeLabel;
    QLabel *timeLeftLabel;
    QSlider *sliderControl;

    AImage *cover;
    QLabel *artistLabel;
    QLabel *titleLabel;

    AButton *playButton;
    AButton *pauseButton;
    AButton *nextButton;
    AButton *previousButton;
    AButton *repeatButton;
    AButton *shuffleButton;
    AButton *addAudioButton;
    AButton *downloadButton;

    QMediaPlayer *mediaPlayer;

    bool sliderPressed;
    bool repeatTrack;
    bool randomTrack;

    AButton *addButton(QString iconPath, QSize size);

    PlayListItem track;
public:
    explicit AudioPlayer(QWidget *parent = 0);

signals:
    void nextTrack(bool shuffle);
    void previousTrack(bool shuffle);
    void newDownload(QString artist, QString title, QString url);

public slots:
    void setTrack(PlayListItem track);

    void playButtonClicked();
    void pauseButtonClicked();

    void nextButtonClicked();
    void previousButtonClicked();

    void repeatButtonClicked(bool state);
    void shuffleButtonClicked(bool state);

    void addAudioButtonClicked();
    void downloadButtonClicked();

    void positionChanged(qint64 pos);

    void sliderControlPress();
    void sliderControlRelease();
    void sliderControlMoved(int pos);

};

#endif // AUDIOPLAYER_H
