#-------------------------------------------------
#
# Project created by QtCreator 2013-11-06T10:31:48
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

QT += webkitwidgets \
      network \
      xml \
      multimedia

TARGET = KURSACH
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    AFramework/abutton.cpp \
    AFramework/aimage.cpp \
    AFramework/aframe.cpp \
    mainframe.cpp \
    authframe.cpp \
    AFramework/aconnectdialog.cpp \
    AFramework/avkmanager.cpp \
    AFramework/alineedit.cpp \
    musictab.cpp \
    downloadtab.cpp \
    menulist.cpp \
    playlist.cpp \
    audioplayer.cpp \
    downloadlistitem.cpp

HEADERS  += mainwindow.h \
    AFramework/abutton.h \
    AFramework/aimage.h \
    AFramework/aframe.h \
    mainframe.h \
    authframe.h \
    AFramework/aconnectdialog.h \
    AFramework/avkmanager.h \
    AFramework/alineedit.h \
    musictab.h \
    downloadtab.h \
    menulist.h \
    playlist.h \
    audioplayer.h \
    downloadlistitem.h

RESOURCES += \
    AFramework/data.qrc
