#include "downloadtab.h"

DownloadTab::DownloadTab(QWidget *parent) :
    QWidget(parent)
{
    mainLayout = new QVBoxLayout(this);
    mainLayout->setContentsMargins(0,0,0,0);
    mainLayout->setSpacing(0);
    bodyWidget = new QWidget(this);
    mainLayout->addWidget(bodyWidget);

    bodyLayout = new QVBoxLayout(bodyWidget);
    bodyLayout->setContentsMargins(5,5,5,5);

    clenButton = new AButton("ОЧИСТИТЬ","",QFont("a_CityNova",14),MenuButton);
    clenButton->setFixedSize(clenButton->getTextWidth(),40);
    bodyLayout->addWidget(clenButton,0,Qt::AlignLeft);
    connect(clenButton, SIGNAL(clicked()), this, SLOT(cleanList()));

    listWidget = new QListWidget(bodyWidget);
    listWidget->setFrameStyle(QFrame::NoFrame | QFrame::Raised);
    listWidget->setStyleSheet("QListWidget { background: rgba(255,255,255,0); padding: 10px;}"
                              "QListWidget::item { height: 50px; margin: 5px;}"
                              "QListView::item {background: rgba(255,255,255,20); }"
                              "QListView::item:hover {background: rgba(255,255,255,30); }"
                              "QListView::item:selected:active {background: rgba(255,255,255,40);}"
                              "QListView::item:selected:!active {background: rgba(255,255,255,40);}");

    QScrollBar *listWidgetVerticalScrollBar = listWidget->verticalScrollBar();

    listWidgetVerticalScrollBar->setStyleSheet("QScrollBar:vertical  {"
                                             "background: rgba(190,190,190,0); "
                                             "width: 15px}"
                                             "QScrollBar::add-line:vertical  {"
                                             "width: 0px;"
                                             "height: 0px;}"
                                             "QScrollBar::sub-line:vertical  {"
                                             "width: 0px;"
                                             "height: 0px;}"
                                             "QScrollBar::handle:vertical  {"
                                             "background: rgba(255,255,255,100);"
                                             "margin: 5px;"
                                             "width: 10px;}"
                                             "QScrollBar::handle:vertical:hover  {"
                                             "background: rgba(255,255,255,240);"
                                             "margin: 5px;"
                                             "width: 10px;}");

    bodyLayout->addWidget(listWidget);
    connect(listWidget, SIGNAL(itemClicked(QListWidgetItem*)), this, SLOT(itemClicked(QListWidgetItem*)));

}

void DownloadTab::addDownload(QString artist, QString title, QString url)
{
    QListWidgetItem *item = new QListWidgetItem();
    listWidget->addItem(item);


    DownloadListItem *itemWidget = new DownloadListItem(QString::number(listWidget->row(item)+1),artist, title, url, listWidget);
    listWidget->setItemWidget(item, itemWidget);
}

void DownloadTab::itemClicked(QListWidgetItem *item)
{
    DownloadListItem *itemWidget = (DownloadListItem *)listWidget->itemWidget(item);
    if(itemWidget->downloaded)
    {
        if(QDesktopServices::openUrl(QUrl(itemWidget->filePath)))
        {
            itemWidget->loadStatus->setText("ФАЙЛ ОТКРЫТ");
        }else{
            itemWidget->loadStatus->setText("ОШИБКА ПРИ ОТКРЫТИИ ФАЙЛА");
        }
    }
}

void DownloadTab::cleanList()
{
    listWidget->clear();
}
