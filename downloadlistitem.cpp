#include "downloadlistitem.h"

DownloadListItem::DownloadListItem(QString num, QString art, QString tit, QString url, QWidget *parent) :
    QWidget(parent)
{
    this->artist = art;
    this->title = tit;
    this->url = url;
    downloaded = false;

    mainLayout = new QVBoxLayout(this);
    mainLayout->setContentsMargins(0,0,0,0);
    mainLayout->setSpacing(0);
    bodyWidget = new QWidget(this);
    mainLayout->addWidget(bodyWidget);

    bodyLayout = new QVBoxLayout(bodyWidget);
    bodyLayout->setContentsMargins(5,5,5,5);

    QHBoxLayout *trackNameLayout = new QHBoxLayout();
    bodyLayout->addLayout(trackNameLayout);

    numberInList = new QLabel(num+".");
    numberInList->setFont(QFont("a_CityNova",12));
    numberInList->setStyleSheet("color: white;");
    trackNameLayout->addWidget(numberInList,0,Qt::AlignLeft);

    trackName = new QLabel(art.toUpper()+" - "+tit.toUpper());
    trackName->setFont(QFont("a_CityNova",12));
    trackName->setStyleSheet("color: white;");
    trackNameLayout->addWidget(trackName,0,Qt::AlignLeft);

    loadStatus = new QLabel("...");
    loadStatus->setFont(QFont("a_CityNova",12));
    loadStatus->setStyleSheet("color: white;");
    trackNameLayout->addWidget(loadStatus,1,Qt::AlignRight);

    progressBar = new QProgressBar();
    progressBar->setFixedHeight(5);
    progressBar->setValue(50);
    progressBar->setRange(0,99);
    progressBar->setStyleSheet("QProgressBar {background-color: rgba(255,255,255,255); }"
                               "QProgressBar::chunk {"
                               "background-color: rgba(222,0,0,255);"
                               "width: 10px;"
                               "}");
    bodyLayout->addWidget(progressBar,Qt::AlignLeft);
    setFixedHeight(50);



    fileName = artist+" - "+title+".mp3";
    QUrl u = QUrl(url);
    QNetworkRequest request(u);
    if(fileName.isEmpty())
        fileName = u.path();
    filePath =  QFileDialog::getSaveFileName(0,"Сохранить файл...",fileName);

    manager = new QNetworkAccessManager(this);
    QObject::connect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(downloadFinished(QNetworkReply*)));

    if(!filePath.isEmpty())
        QObject::connect(manager->get(request), SIGNAL(downloadProgress(qint64,qint64)), this, SLOT(downloadProgress(qint64,qint64)));


    time = 0;
    connect(&timer, SIGNAL(timeout()), this, SLOT(tick()));
    //time.start();
    timer.start(1000);
}

void DownloadListItem::downloadFinished(QNetworkReply *data)
{
    QFile localFile(filePath.toLatin1());
    if (!localFile.open(QIODevice::WriteOnly))
        return;
    const QByteArray sdata = data->readAll();
    localFile.write(sdata);
    localFile.close();
    downloaded = true;
    loadStatus->setText("ФАЙЛ ЗАГРУЖЕН");
    timer.stop();
    progressBar->setValue(99);
}

void DownloadListItem::downloadProgress(qint64 recieved1, qint64 total)
{
    progressBar->setValue(100*recieved1/total);
    loadStatus->setText(QString::number(100*recieved1/total)+"%, "+timerString);
}

void DownloadListItem::tick()
{
    time++;

    if((time%60) < 10)
        timerString = QString("0") + QString::number(time%60);
    else
        timerString = QString::number(time%60);

    timerString = QString::number(time/60)+":"+timerString;
    timer.start(1000);
    qDebug() << timerString;
}
