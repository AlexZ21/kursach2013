#include "alineedit.h"

ALineEdit::ALineEdit(QWidget *parent) :
    QLineEdit(parent)
{
    setStyleSheet("color: rgba(235,235,235,255); border-image: url(:/data/line_out.png) 0 5 5 fill stretch; border-style: solid; border-width: 5px;");
    font.setPointSize(12);
    setFont(font);
}

void ALineEdit::focusInEvent(QFocusEvent *ev)
{
    setStyleSheet("color: rgba(235,235,235,255); border-image: url(:/data/line_in.png) 0 5 5 fill stretch; border-style: solid; border-width: 5px;");
    repaint();
}

void ALineEdit::focusOutEvent(QFocusEvent *ev)
{
    setStyleSheet("color: rgba(235,235,235,255); border-image: url(:/data/line_out.png) 0 5 5 fill stretch; border-style: solid; border-width: 5px;");
    repaint();
}
