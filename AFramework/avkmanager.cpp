#include "avkmanager.h"

AVKManager AVKManager::manager;

void AVKManager::setToken(QString token)
{
    manager.token = token;
    QFile file;
    file.setFileName("token");
    file.open(QIODevice::WriteOnly);
    QTextStream text(&file);
    text << token;
    file.close();
}

void AVKManager::setId(QString id)
{
    manager.id = id;
    QFile file;
    file.setFileName("token");
    file.open(QIODevice::Append);
    QTextStream text(&file);
    text << "\n" << id;
    file.close();
}

bool AVKManager::checkToken()
{
    QFile file;
    file.setFileName("token");
    file.open(QIODevice::ReadOnly);
    QTextStream text(&file);
    manager.token = text.readLine();
    manager.id = text.readLine();
    file.close();

    if(manager.token.isEmpty())
        return false;
    else
        return true;

}

void AVKManager::clearToken()
{
    QFile file;
    file.setFileName("token");
    file.open(QIODevice::WriteOnly);
    QTextStream text(&file);
    text <<"";
    file.close();
    manager.token = "";
}

void AVKManager::get(QString method, QString param)
{
    AVKManager::manager.lastQuery = QUrl::fromUserInput("https://api.vk.com/method/"+method+".xml?"+param+"&v=5.0&access_token="+AVKManager::manager.token);
    AVKManager::manager.reply = AVKManager::manager.netManager->get(QNetworkRequest(AVKManager::manager.lastQuery));
    AVKManager::manager.lastMethod = method;
    QObject::connect(AVKManager::manager.reply,SIGNAL(finished()),&AVKManager::manager, SLOT(loadFinished()));
}

QString AVKManager::getId()
{
    return manager.id;
}

AVKManager *AVKManager::getManager()
{
    return &manager;
}

void AVKManager::loadFinished()
{
    QDomDocument doc;
    doc.setContent(reply->readAll());
    delete reply;
    reply = NULL;
    QUrlQuery urlQuery(AVKManager::manager.lastQuery.query());
    urlQuery.removeQueryItem("v");
    urlQuery.removeQueryItem("access_token");


    AVKReply r;
    r.method = lastMethod;
    r.document = doc;
    r.ownerId = urlQuery.queryItemValue("owner_id");
    r.query = urlQuery;
    //emit getFinished(lastMethod, urlQuery, doc);
    emit getFinished(r);
}

AVKManager::AVKManager(QObject *parent) : QObject(parent)
{
    netManager = new QNetworkAccessManager(this);
    reply = NULL;
    qDebug() << "Init VK manager";
}

AVKManager::~AVKManager()
{
    delete netManager;
    qDebug() << "Destroy VK manager";
}
