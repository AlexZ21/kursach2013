#include "aframe.h"


AFrame::AFrame(AFrame *parentFrame, QWidget *parent) :
    QWidget(parent)
{
    mainLayout = new QVBoxLayout(this);
    mainLayout->setContentsMargins(0,0,0,0);
    mainLayout->setSpacing(0);

    headerWidget = new QWidget(this);
    headerWidget->setFixedHeight(44);
    headerWidget->setStyleSheet("QWidget {background: rgba(0,0,0,150);}");

    headerLayout = new QHBoxLayout(headerWidget);
    headerLayout->setContentsMargins(0,0,0,0);
    headerLayout->setSpacing(2);

    backButton = new AButton(headerWidget);
    backButton->setImage(":/data/back");
    backButton->setImageSize(QSize(30,40));
    backButton->setBackgroundOnlyFocus(true);
    backButton->setFixedSize(30,40);
    connect(backButton, SIGNAL(clicked()), this, SLOT(close()));
    headerLayout->addWidget(backButton,0,Qt::AlignLeft | Qt::AlignVCenter);

    headerLayout->addSpacing(2);

    headerIcon = new AImage(headerWidget);
    headerIcon->setFixedSize(40,40);
    headerIcon->loadFromFile("data/test");
    headerLayout->addWidget(headerIcon,0,Qt::AlignLeft | Qt::AlignVCenter);

    headerTitle = new QLabel(headerWidget);
    headerTitle->setText("Новый фрейм");
    headerTitle->setFont(QFont("a_CityNova",14,QFont::Bold));
    headerTitle->setStyleSheet("QLabel {color: rgba(255,255,255,255);}");
    headerLayout->addWidget(headerTitle,1,Qt::AlignLeft | Qt::AlignVCenter);

    if(parentFrame != NULL)
    {
        this->parentFrame = parentFrame;
        layout = this->parentFrame->getLayout();
        parentFrame->hide();
        layout->addWidget(this);
    }else{
        this->parentFrame = NULL;
        layout = NULL;
        backButton->hide();
    }

    mainLayout->addWidget(headerWidget, 0, Qt::AlignTop);
}

QLabel *AFrame::getHeaderTitle()
{
    return headerTitle;
}

AImage *AFrame::getHeaderIcon()
{
    return headerIcon;
}

void AFrame::addWidgetToHeader(QWidget *widget)
{
    headerLayout->addWidget(widget,0,Qt::AlignRight | Qt::AlignVCenter);
}

void AFrame::setInLayout(QLayout *layout)
{
    this->layout = layout;
    layout->addWidget(this);
}

QLayout *AFrame::getLayout()
{
    return layout;
}

void AFrame::hideBackButton(bool h)
{
    if(h)
        backButton->hide();
    else
        backButton->show();
}

void AFrame::hideHeaderIcon(bool h)
{
    if(h)
        headerIcon->hide();
    else
        headerIcon->show();
}

void AFrame::setDeleteOnClose(bool d)
{
    setAttribute(Qt::WA_DeleteOnClose, d);
}

QVBoxLayout *AFrame::getMainLayout()
{
    return mainLayout;
}

void AFrame::closeEvent(QCloseEvent *e)
{
    AFrame *fp = parentFrame;
    close();
    fp->show();
}
