#include "abutton.h"

AButton::AButton(QWidget *parent):QWidget(parent)
{
    press = 10;
    focus = 0;
    text = "";
    size.setHeight(0);
    size.setWidth(0);
    drawBackground = true;
    drawBackgroundOnlyFocus = false;
    backgroundAlpha = 255;
    checked = false;
    checkable = false;
    type = DefaultButton;
}

AButton::AButton(QString text, QString iconPath, QFont font, AButtonType type)
{
    press = 10;
    focus = 0;
    this->text = text;
    this->size.setHeight(0);
    this->size.setWidth(0);
    drawBackground = true;
    drawBackgroundOnlyFocus = false;
    backgroundAlpha = 255;
    checked = false;
    checkable = false;
    this->type = type;
    setFont(font);
    if(!iconPath.isEmpty()) setImage(iconPath);
}

void AButton::paintEvent(QPaintEvent *e)
{
    QPainter p(this);

    p.setRenderHint(QPainter::Antialiasing);
    p.setRenderHint(QPainter::SmoothPixmapTransform);


    QPen pen;
    pen.setColor(QColor(0,0,0,0));
    p.setPen(pen);

    pen.setColor(QColor(225+press+focus,225+press+focus,225+press+focus,255));
    p.setPen(pen);

    p.setFont(font());
    QFontMetrics fontMetric(font());

    int fontHeight = 0;
    if(!text.isEmpty()) fontHeight= fontMetric.height();

    switch (type) {
    case DefaultButton:
        p.setBrush(QBrush(QColor(press+focus,press+focus,press+focus,backgroundAlpha*0.6)));
        if(drawBackground) p.drawRoundedRect(4, 4, width()-8, height()-8, 5, 5);
        p.drawText(QRect(8, 10+size.height(), width()-16, height()-20-size.height()),text,QTextOption(Qt::AlignCenter));
        break;
    case MenuButton:
        if(drawBackground){
            if((press+focus) == 10)
            {
                p.drawText(QRect(8, 10+size.height(), width()-16, height()-20-size.height()),text,QTextOption(Qt::AlignCenter));
            }

           if((press+focus) == 0 || (press+focus) == 15)
           {
               p.setBrush(QBrush(QColor(170,0,0,backgroundAlpha)));
               pen.setColor(QColor(0,0,0,0));
               p.setPen(pen);
               pen.setColor(QColor(225+press+focus,225+press+focus,225+press+focus,255));
               if(!text.isEmpty())
                   p.drawRect(QRect(4,height()/2-(fontHeight*1.2)/2,width()-8,(fontHeight*1.2)));
               else
                   p.drawRect(QRect(width()/2-size.width()/2,height()/2-size.height()/2,size.width(),size.height()));
               pen.setColor(QColor(225+press+focus,225+press+focus,225+press+focus,255));
               p.setPen(pen);
               p.drawText(QRect(8, 10+size.height(), width()-16, height()-20-size.height()),text,QTextOption(Qt::AlignCenter));
           }

           if((press+focus) == 25)
           {
               p.setBrush(QBrush(QColor(222,0,0,backgroundAlpha)));
               pen.setColor(QColor(0,0,0,0));
               p.setPen(pen);
               if(!text.isEmpty())
                   p.drawRect(QRect(4,height()/2-(fontHeight*1.2)/2,width()-8,(fontHeight*1.2)));
               else
                   p.drawRect(QRect(width()/2-size.width()/2,height()/2-size.height()/2,size.width(),size.height()));
               pen.setColor(QColor(225+press+focus,225+press+focus,225+press+focus,255));
               p.setPen(pen);
               p.drawText(QRect(8, 10+size.height(), width()-16, height()-20-size.height()),text,QTextOption(Qt::AlignCenter));
           }
        }
        break;
    case AudioPlayerControl:
        if(drawBackground){
            pen.setColor(QColor(0,0,0,0));
            p.setPen(pen);
           if((press+focus) == 15)
           {
               p.setBrush(QBrush(QColor(0,0,0,backgroundAlpha*0.6)));
               p.drawRoundedRect(2, 2, width()-4, height()-4, 5, 5);
           }
           if((press+focus) == 0)
           {
               p.setBrush(QBrush(QColor(0,0,0,backgroundAlpha*0.6)));
               p.drawRoundedRect(2, 2, width()-4, height()-4, 5, 5);
           }

           if((press+focus) == 25 )
           {
               p.setBrush(QBrush(QColor(0,0,0,backgroundAlpha*0.3)));
               p.drawRoundedRect(2, 2, width()-4, height()-4, 5, 5);
           }
           pen.setColor(QColor(225+press+focus,225+press+focus,225+press+focus,255));
           p.setPen(pen);
           p.drawText(QRect(8, 10+size.height(), width()-16, height()-20-size.height()),text,QTextOption(Qt::AlignCenter));
        }
        break;
    default:
        p.setBrush(QBrush(QColor(press+focus,press+focus,press+focus,backgroundAlpha*0.6)));
        if(drawBackground) p.drawRoundedRect(4, 4, width()-8, height()-8, 5, 5);
        p.drawText(QRect(8, 10+size.height(), width()-16, height()-20-size.height()),text,QTextOption(Qt::AlignCenter));
        break;
    }



    pen.setColor(QColor(0,0,0,0));
    p.setPen(pen);

    if(!image.isNull() && (press+focus) == 10) p.drawImage(QRect(width()/2-size.width()/2,
                                                                 (height()-fontHeight)/2-size.height()/2,
                                                                 size.width(), size.height()),image);

    if(!imagePress.isNull() && (press+focus) == 0 || (press+focus) == 15) p.drawImage(QRect(width()/2-size.width()/2,
                                                                     (height()-fontHeight)/2-size.height()/2,
                                                                     size.width(), size.height()),imagePress);

    if(!imageFocus.isNull() && (press+focus) == 25) p.drawImage(QRect(width()/2-size.width()/2,
                                                                      (height()-fontHeight)/2-size.height()/2,
                                                                      size.width(), size.height()),imageFocus);


}

void AButton::leaveEvent(QEvent *e)
{
    if(drawBackgroundOnlyFocus)
        backgroundAlpha = 0;
    focus = 0;
    repaint();
}

void AButton::enterEvent(QEvent *e)
{
    if(drawBackgroundOnlyFocus) backgroundAlpha = 255;
    focus = 15;
    repaint();
}

void AButton::mousePressEvent(QMouseEvent *e)
{
    if(drawBackgroundOnlyFocus) backgroundAlpha = 255;

    if(checkable)
    {
        if(checked)
            checked = false;
        else
            checked = true;
    }

    press = -15;
    repaint();
}

void AButton::mouseReleaseEvent(QMouseEvent *e)
{
    if(drawBackgroundOnlyFocus)
    {
        if(focus == 15)
            backgroundAlpha = 255;
        else
            backgroundAlpha = 0;
    }

    if(checked)
    {
        press = 0;
    }else{
        press = 10;
    }

    emit clicked();
    emit changeState(checked);
    emit clicked(this);

    repaint();
}

void AButton::setText(QString text)
{
    this->text = text;
}

void AButton::setImage(QString path)
{
    image.load(path);
    if(!image.isNull())
    {
        size = image.size();
        if(imageFocus.isNull()) imageFocus = setBrightness(image, 20);
        if(imagePress.isNull())  imagePress = setBrightness(image, -20);
    }
}

void AButton::setImageFocus(QString path)
{
    imageFocus.load(path);
}

void AButton::setImagePress(QString path)
{
    imagePress.load(path);
}

void AButton::setImageSize(QSize size)
{
    this->size = size;
}

void AButton::setBackground(bool enable)
{
    drawBackground = enable;
}

void AButton::setBackgroundOnlyFocus(bool enable)
{
    drawBackgroundOnlyFocus = enable;
    if(drawBackgroundOnlyFocus)
    {
        backgroundAlpha = 0;
        repaint();
    }else{
        backgroundAlpha = 100;
        repaint();
    }
}

void AButton::setCheckable(bool checkable)
{
    this->checkable = checkable;
}

void AButton::setChecked(bool check)
{
    checked = check;

    if(checked)
    {
        press = 0;
    }else{
        press = 10;
    }

    repaint();
}

void AButton::setButtonType(AButtonType type)
{
    this->type = type;
}

int AButton::getTextWidth()
{
    QFontMetrics fm(font());
    return fm.width(text)+0.8*fm.height();
}


QImage AButton::setBrightness(QImage &image, int factor)
{
    if (factor == 0)
        return image;

    QImage img = image;

    int bytes_per_pixel = img.bytesPerLine() / img.width();
    uchar *pixel = NULL;
    QRgb *rgba;

    if (factor > 0) { // lighter
        factor += 100;
        for (int h = 0; h < img.height(); h++) {
            pixel = img.scanLine(h);
            for (int w = 0; w < img.width(); w++) {
                rgba = (QRgb *)pixel;
                if (qAlpha(*rgba) != 0 && (qRed(*rgba) != 0 || qGreen(*rgba) != 0 || qBlue(*rgba) != 0))
                    *rgba = QColor::fromRgba(*rgba).lighter(factor).rgba();
                pixel += bytes_per_pixel;
            }
        }
    } else { // darker
        factor = -factor;
        factor += 100;
        for (int h = 0; h < img.height(); h++) {
            uchar *pixel = img.scanLine(h);
            for (int w = 0; w < img.width(); w++) {
                rgba = (QRgb *)pixel;
                if (qAlpha(*rgba) != 0 && (qRed(*rgba) != 0 || qGreen(*rgba) != 0 || qBlue(*rgba) != 0))
                    *rgba = QColor::fromRgba(*rgba).darker(factor).rgba();
                pixel += bytes_per_pixel;
            }
        }
    }

    return img;
}
