#include "aimage.h"

AImage::AImage(QWidget *parent) :
    QWidget(parent)
{
    eventLoop = new QEventLoop(this);
    netMan = new QNetworkAccessManager(this);
    connect(netMan, SIGNAL(finished(QNetworkReply*)), eventLoop, SLOT(quit()));
    animationTimer = new QTimer(this);
    connect(animationTimer, SIGNAL(timeout()), this, SLOT(rotateImage()));
    angle = 0;
}

void AImage::loadFromFile(QString path)
{
    image.load(path);
    repaint();
}


void AImage::paintEvent(QPaintEvent *)
{
    QPainter p(this);
    p.setRenderHint(QPainter::Antialiasing, true);
    p.setRenderHint(QPainter::SmoothPixmapTransform, true);
    p.translate(width()/2,height()/2);
    p.rotate(angle);
    p.drawImage(QRect(-width()/2,-height()/2,width(),height()),image);
}

void AImage::rotateImage()
{
    angle+=2;
    repaint();
    animationTimer->start(5);
}

void AImage::start()
{
    animationTimer->start(5);
}

void AImage::stop()
{
    animationTimer->stop();
}


void AImage::loadFromUrl(QString path)
{
    QNetworkRequest request;
    request.setUrl(path);
    QNetworkReply *reply = netMan->get(request);
    eventLoop->exec();

    QImageReader reader(reply);
    if (reply->error() == QNetworkReply::NoError)
        reader.read(&image);
    repaint();
}

void AImage::setRounded(int radius)
{
    QImage shapeImg(image.size(), QImage::Format_ARGB32_Premultiplied);
    shapeImg.fill(Qt::transparent);
    QPainter sp(&shapeImg);
    sp.setRenderHint(QPainter::Antialiasing);
    sp.setPen(QPen(Qt::color1));
    sp.setBrush(QBrush(Qt::white));
    sp.drawRoundedRect(QRect(0, 0, image.width(), image.height()), radius + 1, radius + 1);
    sp.end();
    QImage roundSquaredImage(image.size(), QImage::Format_ARGB32_Premultiplied);
    roundSquaredImage.fill(Qt::transparent);
    QPainter p(&roundSquaredImage);
    p.setRenderHint(QPainter::Antialiasing);
    p.drawImage(0, 0, shapeImg);
    p.setCompositionMode(QPainter::CompositionMode_SourceIn);
    p.drawImage(0, 0, image);
    p.end();
    image = roundSquaredImage;


    repaint();
}

bool AImage::isActive()
{
    return animationTimer->isActive();
}
