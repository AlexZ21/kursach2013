#ifndef AIMAGE_H
#define AIMAGE_H

#include <QWidget>
#include <QEventLoop>
#include <QNetworkAccessManager>
#include <QImageReader>
#include <QImage>
#include <QNetworkReply>
#include <QNetworkRequest>
#include <QPainter>
#include <QTimer>

class AImage : public QWidget
{
    Q_OBJECT
    QImage image;
    QEventLoop *eventLoop;
    QNetworkAccessManager *netMan;
    QTimer *animationTimer;
    int angle;
public:
    explicit AImage(QWidget *parent = 0);
    void loadFromFile(QString path);
    void loadFromUrl(QString path);
    void setRounded(int radius = 3);
    bool isActive();
protected:
    void paintEvent(QPaintEvent *);
public slots:
    void rotateImage();
    void start();
    void stop();
    
};

#endif // AIMAGE_H
