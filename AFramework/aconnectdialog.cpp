#include "aconnectdialog.h"

AConnectDialog::AConnectDialog(QWidget *parent) :
    QWebView(parent)
{
    connect(this, SIGNAL(urlChanged(QUrl)),SLOT( slotUrlChanged(QUrl)));
}

void AConnectDialog::loadLoginPage()
{
    QUrl url("https://oauth.vk.com/authorize");
    QUrlQuery urlQuery;

    urlQuery.addQueryItem("client_id",  "3872150");
    urlQuery.addQueryItem("display", "popup");
    urlQuery.addQueryItem("scope", "photos,offline,audio,wall,friends");
    urlQuery.addQueryItem("response_type", "token");

    url.setQuery(urlQuery);
    load(url);
}

void AConnectDialog::slotUrlChanged(QUrl url)
{
    url = url.toString().replace("#","?");
    QUrlQuery urlQuery(url.query());

    if (urlQuery.hasQueryItem("error"))
        return;

    if (urlQuery.hasQueryItem("access_token"))
    {
        emit connected(urlQuery.queryItemValue("access_token"), urlQuery.queryItemValue("user_id"));
    }
}
