#ifndef AFRAME_H
#define AFRAME_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QLabel>
#include "abutton.h"
#include "aimage.h"

class AFrame : public QWidget
{
    Q_OBJECT
    QVBoxLayout *mainLayout;

    QWidget *headerWidget;
    QHBoxLayout *headerLayout;

    QLabel *headerTitle;
    AImage *headerIcon;

    AButton *backButton;

    AFrame *parentFrame;

    QLayout *layout;

public:
    explicit AFrame(AFrame *parentFrame, QWidget *parent = 0);

    QLabel *getHeaderTitle();
    AImage *getHeaderIcon();

    void addWidgetToHeader(QWidget *widget);

    void setInLayout(QLayout *layout);
    QLayout *getLayout();

    void hideBackButton(bool h);
    void hideHeaderIcon(bool h);

    void setDeleteOnClose(bool d);

    QVBoxLayout *getMainLayout();

protected:

    void closeEvent(QCloseEvent *e);

};

#endif // AFRAME_H
