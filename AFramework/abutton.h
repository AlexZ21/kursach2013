#ifndef ABUTTON_H
#define ABUTTON_H

#include <QWidget>
#include <QPainter>
#include <QFontMetrics>
#include <QDebug>

enum AButtonType
{
    DefaultButton,
    MenuButton,
    AudioPlayerControl
};


class AButton : public QWidget
{
    Q_OBJECT

    int press;
    int focus;
    QString text;
    QSize size;
    bool drawBackground;
    bool drawBackgroundOnlyFocus;
    int backgroundAlpha;
    bool checked;
    bool checkable;
    AButtonType type;


    QImage image;
    QImage imagePress;
    QImage imageFocus;

    QImage setBrightness(QImage &image, int factor);
public:
    AButton(QWidget *parent = 0);
    AButton(QString text, QString iconPath, QFont font, AButtonType type);
    void setText(QString text);
    void setImage(QString path);
    void setImageFocus(QString path);
    void setImagePress(QString path);
    void setImageSize(QSize size);
    void setBackground(bool enable);
    void setBackgroundOnlyFocus(bool enable);
    void setCheckable(bool checkable);
    void setChecked(bool check);
    void setButtonType(AButtonType type);
    int getTextWidth();
protected:
    void paintEvent(QPaintEvent *e);

    void enterEvent(QEvent *e);
    void leaveEvent(QEvent *e);

    void mousePressEvent(QMouseEvent *e);
    void mouseReleaseEvent(QMouseEvent *e);
signals:
    void clicked();
    void changeState(bool state);
    void clicked(AButton *b);
};

#endif // ABUTTON_H
