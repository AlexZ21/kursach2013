#ifndef AVKMANAGER_H
#define AVKMANAGER_H

#include <QDebug>
#include <QFile>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QtXml/QDomDocument>
#include <QObject>
#include <QUrl>
#include <QUrlQuery>

#define VKManager AVKManager::getManager()

struct AVKReply
{
    QString method;
    QString ownerId;
    QUrlQuery query;
    QDomDocument document;
};

class AVKManager : public QObject
{
    Q_OBJECT
    QString token;
    QString id;
public:
    static void setToken(QString token);
    static void setId(QString id);
    static bool checkToken();
    static void clearToken();
    static void get(QString method, QString param);
    static QString getId();
    static AVKManager *getManager();
public slots:
    void loadFinished();
signals:
    void getFinished(QString method, QUrlQuery query, QDomDocument dom);
    void getFinished(AVKReply reply);
private:
    AVKManager(QObject *parent = 0);
    ~AVKManager();
    QNetworkReply *reply;
    QNetworkAccessManager *netManager;
    QString lastMethod;
    QUrl lastQuery;
    static AVKManager manager;
};

#endif // AVKMANAGER_H
