#ifndef ALINEEDIT_H
#define ALINEEDIT_H


#include <QLineEdit>
#include <QFont>
#include <QKeyEvent>

class ALineEdit : public QLineEdit
{
    Q_OBJECT
    QFont font;
public:
    explicit ALineEdit(QWidget *parent = 0);
protected:
    void focusInEvent(QFocusEvent *ev);
    void focusOutEvent(QFocusEvent *ev);
};

#endif // ALINEEDIT_H
