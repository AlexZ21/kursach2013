#ifndef ACONNECTDIALOG_H
#define ACONNECTDIALOG_H

#include <QtWebKitWidgets/QWebView>
#include <QUrl>
#include <QUrlQuery>
#include <QDebug>

class AConnectDialog : public QWebView
{
    Q_OBJECT
public:
    AConnectDialog(QWidget *parent = 0);
    void loadLoginPage();
signals:
    void connected(QString token, QString id);
public slots:
    void slotUrlChanged(QUrl url);
};

#endif // ACONNECTDIALOG_H
