#ifndef MENULIST_H
#define MENULIST_H

#include <QListWidget>
#include <QScrollBar>
#include <QDebug>
#include <QVector>
#include <QStack>

#include "AFramework/avkmanager.h"

struct MenuListItem
{
    QString type;
    QVector<QString> data;
};

class MenuList : public QListWidget
{
    Q_OBJECT
    QStack< QVector<MenuListItem> > stackList;
public:
    explicit MenuList(QWidget *parent = 0);
    void setMenu();
    void updateMenuList();
    void setGroupsList(QDomNode node);
    void setFriendsList(QDomNode node);
    void setSubMenuList(QString id);

    void addTestList();
signals:

public slots:
    void rowClicked(QListWidgetItem *item);

};

#endif // MENULIST_H
