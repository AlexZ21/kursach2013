#ifndef AUTHFRAME_H
#define AUTHFRAME_H

#include "AFramework/aframe.h"
#include "AFramework/aconnectdialog.h"
#include "AFramework/avkmanager.h"

class AuthFrame : public AFrame
{
    Q_OBJECT
public:
    explicit AuthFrame(AFrame *parentFrame, QWidget *parent = 0);

signals:
    void connected();
public slots:
    void conected(QString token, QString id);
};

#endif // AUTHFRAME_H
