#include "mainframe.h"

MainFrame::MainFrame(AFrame *parentFrame, QWidget *parent) :
    AFrame(parentFrame, parent)
{
    connect(AVKManager::getManager(), SIGNAL(getFinished(AVKReply)),this,SLOT(getFinished(AVKReply)));
    getHeaderTitle()->setText("ПЛЕЕР ВК");

    searchLine = new ALineEdit(this);
    searchLine->setFixedWidth(250);
    searchLine->setFont(QFont("a_CityNova",12));
    addWidgetToHeader(searchLine);
    searchLine->hide();
    connect(searchLine, SIGNAL(returnPressed()), this, SLOT(searchButtonClicked()));

    musicTabButton = new AButton("АУДИОЗАПИСИ","",QFont("a_CityNova",12),MenuButton);
    musicTabButton->setFixedSize(musicTabButton->getTextWidth(),40);
    musicTabButton->setCheckable(true);
    addWidgetToHeader(musicTabButton);
    connect(musicTabButton, SIGNAL(clicked()), this, SLOT(musicTabButtonClicked()));

    downloadTabButton = new AButton("ЗАГРУЗКИ","",QFont("a_CityNova",12),MenuButton);
    downloadTabButton->setFixedSize(downloadTabButton->getTextWidth(),40);
    downloadTabButton->setCheckable(true);
    addWidgetToHeader(downloadTabButton);
    connect(downloadTabButton, SIGNAL(clicked()), this, SLOT(downloadTabButtonClicked()));

    searchButton = new AButton("",":/data/search",QFont(),AudioPlayerControl);
    searchButton->setFixedSize(40,40);
    searchButton->setBackground(false);
    searchButton->setImageSize(QSize(30,30));
    addWidgetToHeader(searchButton);
    connect(searchButton, SIGNAL(clicked()), this, SLOT(searchButtonClicked()));

    AImage *sep = new AImage(this);
    sep->loadFromFile(":/data/sep.png");
    sep->setFixedSize(2,30);
    addWidgetToHeader(sep);

    logoutButton = new AButton("ВЫХОД","",QFont("a_CityNova",14,QFont::Bold),MenuButton);
    logoutButton->setFixedSize(logoutButton->getTextWidth(),40);
    connect(logoutButton, SIGNAL(clicked()),this, SLOT(logoutButtonClicked()));
    addWidgetToHeader(logoutButton);

    loginButton = new AButton("ВХОД","",QFont("a_CityNova",14,QFont::Bold),MenuButton);
    loginButton->setFixedSize(loginButton->getTextWidth(),40);
    connect(loginButton, SIGNAL(clicked()),this, SLOT(loginButtonClicked()));
    addWidgetToHeader(loginButton);

    if(AVKManager::checkToken())
    {
        loginButton->hide();
        AVKManager::get("users.get","fields=photo_50");
    }
    else
        logoutButton->hide();   

    musicTab = new MusicTab(this);
    getMainLayout()->addWidget(musicTab,1);
    musicTabButton->setChecked(true);

    downloadTab = new DownloadTab(this);
    getMainLayout()->addWidget(downloadTab,1);
    downloadTab->hide();

    connect(musicTab->getPlayer(), SIGNAL(newDownload(QString,QString,QString)),
            downloadTab, SLOT(addDownload(QString,QString,QString)));
}

void MainFrame::updateHeader(QDomNode node)
{
    QString header(node.toElement().elementsByTagName("first_name").item(0).toElement().text() + " " +
                   node.toElement().elementsByTagName("last_name").item(0).toElement().text());


    getHeaderIcon()->loadFromUrl(node.toElement().elementsByTagName("photo_50").item(0).toElement().text());
    getHeaderIcon()->setFixedSize(40,40);
    //getHeaderIcon()->setRounded(30);
    getHeaderTitle()->setText(header.toUpper());
}

void MainFrame::loginButtonClicked()
{
    AuthFrame *authFrame = new AuthFrame(this);
    connect(authFrame, SIGNAL(connected()),this, SLOT(connected()));
}

void MainFrame::logoutButtonClicked()
{
    AVKManager::clearToken();
    logoutButton->hide();
    loginButton->show();
}

void MainFrame::connected()
{
    loginButton->hide();
    logoutButton->show();
    AVKManager::get("users.get","fields=photo_50");
}

void MainFrame::getFinished(AVKReply reply)
{
    if(reply.method == "users.get")
    {
        QDomElement rootElement = reply.document.documentElement();
        QDomNode node = rootElement.firstChild();
        if(node.toElement().nodeName() == "user")
        {
            this->updateHeader(node);
        }
    }

    if(reply.method == "groups.get")
    {
        QDomElement rootElement = reply.document.documentElement();
        QDomNode node = rootElement.firstChild().nextSibling().firstChild();
        musicTab->getMenuList()->setGroupsList(node);
    }

    if(reply.method == "friends.get")
    {
        QDomElement rootElement = reply.document.documentElement();
        QDomNode node = rootElement.firstChild().nextSibling().firstChild();
        musicTab->getMenuList()->setFriendsList(node);
    }

    if(reply.method == "audio.get")
    {
        musicTab->getPlayList()->setPlayListFromAudio(reply);
        musicTab->getMenuList()->blockSignals(false);
    }

    if(reply.method == "wall.get")
    {
        musicTab->getPlayList()->setPlayListFromWall(reply);
        musicTab->getMenuList()->blockSignals(false);
    }

    if(reply.method == "newsfeed.get")
    {
        musicTab->getPlayList()->setPlayListFromNews(reply);
        musicTab->getMenuList()->blockSignals(false);
    }
    if(reply.method == "audio.search")
    {
        musicTab->getPlayList()->setPlayListFromSearch(reply);
        musicTab->getMenuList()->blockSignals(false);
    }
}

void MainFrame::musicTabButtonClicked()
{
    downloadTabButton->setChecked(false);
    musicTab->show();
    downloadTab->hide();
}

void MainFrame::downloadTabButtonClicked()
{
    musicTabButton->setChecked(false);
    musicTab->hide();
    downloadTab->show();
}

void MainFrame::searchButtonClicked()
{
    if(musicTabButton->isHidden())
    {
        searchLine->hide();
        musicTabButton->show();
        downloadTabButton->show();
        if(!searchLine->text().isEmpty()) AVKManager::get("audio.search","q="+searchLine->text()+"&count=200");
    }else{
        musicTabButton->hide();
        downloadTabButton->hide();
        searchLine->show();
    }
}
