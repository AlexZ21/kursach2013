#ifndef AVKMANAGER_H
#define AVKMANAGER_H

#include <QDebug>
#include <QFile>
#include <QNetworkAccessManager>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QtXml/QDomDocument>
#include <QObject>

#define VKManager AVKManager::getManager()

class AVKManager
{
    QString token;
    QString id;
public:
    static void setToken(QString token);
    static void setId(QString id);
    static bool checkToken();
    static void clearToken();
    static void get(QString method, QString param);
    static AVKManager *getManager();
private:
    AVKManager();
    ~AVKManager();
    QNetworkReply *reply;
    QString lastMethod;
    static AVKManager manager;
};

#endif // AVKMANAGER_H
