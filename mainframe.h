#ifndef MAINFRAME_H
#define MAINFRAME_H

#include "AFramework/aframe.h"
#include "authframe.h"
#include "downloadtab.h"
#include "musictab.h"

#include "AFramework/alineedit.h"

class MainFrame : public AFrame
{
    Q_OBJECT

    AButton *loginButton;
    AButton *logoutButton;

    AButton *musicTabButton;
    AButton *downloadTabButton;
    AButton *searchButton;

    ALineEdit *searchLine;

    MusicTab *musicTab;
    DownloadTab *downloadTab;

public:
    explicit MainFrame(AFrame *parentFrame, QWidget *parent = 0);
    void updateHeader(QDomNode node);

public slots:
    void loginButtonClicked();
    void logoutButtonClicked();

    void connected();
    void getFinished(AVKReply reply);

    void musicTabButtonClicked();
    void downloadTabButtonClicked();
    void searchButtonClicked();

};

#endif // MAINFRAME_H
