#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

#include <QVBoxLayout>
#include <QWidget>
#include <QFontDatabase>

#include "AFramework/abutton.h"
#include "AFramework/aimage.h"
#include "AFramework/aframe.h"
#include "mainframe.h"


class MainWindow : public QMainWindow
{
    Q_OBJECT

    QWidget *centralWidget;
    QVBoxLayout *mainLayout;

    MainFrame *mainFrame;

public:
    MainWindow(QWidget *parent = 0);
    ~MainWindow();
};

#endif // MAINWINDOW_H
