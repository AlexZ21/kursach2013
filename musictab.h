#ifndef MUSICTAB_H
#define MUSICTAB_H

#include <QWidget>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include "menulist.h"
#include "playlist.h"
#include "audioplayer.h"

class MusicTab : public QWidget
{
    Q_OBJECT
    QWidget *bodyWidget;
    QVBoxLayout *mainLayout;
    QHBoxLayout *bodyLayout;
    MenuList *menuList;
    PlayList *playList;
    AudioPlayer *audioPlayer;
public:
    explicit MusicTab(QWidget *parent = 0);
    MenuList *getMenuList();
    PlayList *getPlayList();
    AudioPlayer *getPlayer();
signals:

public slots:

};

#endif // MUSICTAB_H
