#ifndef PLAYLIST_H
#define PLAYLIST_H

#include <QListWidget>
#include <QVector>
#include <QtXml/QDomDocument>
#include <QDebug>
#include <QUrl>
#include <QScrollBar>

#include "AFramework/avkmanager.h"

struct PlayListItem
{
    QString audioId;
    QString url;
    QString artist;
    QString title;
    QString ownerId;
};

class PlayList : public QListWidget
{
    Q_OBJECT
    QVector<PlayListItem> playList;
    int size;
    int offset;
    QString method;
    QString id;
    QScrollBar *vertScrollBar;
    QString q;
public:
    explicit PlayList(QWidget *parent = 0);

    void setPlayListFromAudio(AVKReply &reply);
    void setPlayListFromSearch(AVKReply &reply);
    void setPlayListFromWall(AVKReply &reply);
    void setPlayListFromNews(AVKReply &reply);
    void updatePlayList();
signals:
    void selectTrack(PlayListItem track);

public slots:
    void rowClicked(QListWidgetItem* item);
    void rowDoubleClicked(QListWidgetItem* item);
    void scrollBarChanged(int i);
    void nextTrack(bool shuffle);
    void previousTrack(bool shuffle);
};

#endif // PLAYLIST_H
