#include "musictab.h"

MusicTab::MusicTab(QWidget *parent) :
    QWidget(parent)
{
    mainLayout = new QVBoxLayout(this);
    mainLayout->setContentsMargins(0,0,0,0);
    mainLayout->setSpacing(0);
    bodyWidget = new QWidget(this);
    mainLayout->addWidget(bodyWidget);
    bodyWidget->setStyleSheet("* {background: rgba(255,255,255,50);}");

    bodyLayout = new QHBoxLayout();
    bodyLayout->setContentsMargins(0,0,0,0);
    mainLayout->addLayout(bodyLayout,1);

    menuList = new MenuList(bodyWidget);
    bodyLayout->addWidget(menuList,0,Qt::AlignLeft);

    playList = new PlayList(bodyWidget);
    bodyLayout->addWidget(playList,1);

    audioPlayer = new AudioPlayer(bodyWidget);
    mainLayout->addWidget(audioPlayer,0,Qt::AlignBottom);

    connect(playList,SIGNAL(selectTrack(PlayListItem)),audioPlayer,SLOT(setTrack(PlayListItem)));

    connect(audioPlayer,SIGNAL(nextTrack(bool)),playList,SLOT(nextTrack(bool)));
    connect(audioPlayer,SIGNAL(previousTrack(bool)),playList,SLOT(previousTrack(bool)));
}

MenuList *MusicTab::getMenuList()
{
    return menuList;
}

PlayList *MusicTab::getPlayList()
{
    return playList;
}

AudioPlayer *MusicTab::getPlayer()
{
    return audioPlayer;
}
